class Tab {
  constructor(tabsList) {
    this.tabsList = tabsList;
  }

  addToThePage(tabBar) {
    let id = 0;
    this.tabsList.forEach(tabInfo => {
      id = this.tabsList.indexOf(tabInfo);
      tabBar.appendChild(this.tabRender(tabInfo, id));
    })
  }

  // static
  tabRender(tabInfo, id) {
    const tabElem = document.createElement("a");
    tabElem.textContent = tabInfo.title;
    tabElem.classList.add('tab__item');
    tabElem.id = id;
    tabElem.addEventListener('click', () => this.clickHandler(id));

    return tabElem;
  }

  // static
  clickHandler(id) {
    for (let i = 0; i < this.tabsList.length; i++) {
      if (i === id) {
        document.getElementById(String(i)).classList.add('_current');
      } else {
        document.getElementById(String(i)).classList.remove('_current');
      }
    }
    const tabContent = document.querySelector('.tab-content');
    tabContent.textContent = this.tabsList[id].data;
    console.log(this.tabsList[id].data);
  }
}

// Входные данные
const tab = new Tab([
  {
    title: 'Общая информация',
    data: `Какая-то личная информация о пользователе`
  },
  {
    title: 'История',
    data: `Какая-то история действий пользователя`
  },
  {
    title: 'Друзья',
    data: `Какие-то люди`
  }
])

const tabBar = document.getElementById('tab');
tab.addToThePage(tabBar);